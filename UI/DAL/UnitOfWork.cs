﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UI.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public ITodoEfRepository Todos { get; private set; }


        public UnitOfWork(DbContext dbContext)
        {
            _context = dbContext;
            Todos = new TodoEfRepository(_context);
        }

        public int Process()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}