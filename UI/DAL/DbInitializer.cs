﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Models;

namespace UI.DAL
{
    public class DbInitializer : //System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
                                   System.Data.Entity.DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var todos = new List<ToDo>
            {
                new ToDo{ TablePosition=3, Topic="First Topic", TopicToDisplay="First Topic",
                          Activity ="First Activity", ActivityToDisplay="First Activity",
                          Description="First Description", DescriptionToDisplay="First Description",
                          StartDate =DateTime.Parse("2019/1/2"), EndDate=DateTime.Parse("2019/7/28"),
                          PercentageOfCompletion=12, Priority=1, Status=1 },
                new ToDo{ TablePosition=4, Topic="Second Topic", TopicToDisplay="Second Topic",
                          Activity ="Second Activity", ActivityToDisplay="Second Activity",
                          Description ="Second Description", DescriptionToDisplay="Second Descriptio...",
                          StartDate =DateTime.Parse("2019/2/2"), EndDate=DateTime.Parse("2019/8/28"),
                          PercentageOfCompletion=23, Priority=2, Status=3 },
                new ToDo{ TablePosition=6, Topic="Thiird Topic", TopicToDisplay="Thiird Topic",
                          Activity ="Third Activity", ActivityToDisplay ="Third Activity",
                          Description ="Third Description", DescriptionToDisplay ="Third Description",
                          StartDate =DateTime.Parse("2019/3/2"), EndDate=DateTime.Parse("2019/9/28"),
                          PercentageOfCompletion=34, Priority=2, Status=1 },
                new ToDo{ TablePosition=7, Topic="Fourth Topic", TopicToDisplay="Fourth Topic",
                          Activity ="Fourth Activity", ActivityToDisplay ="Fourth Activity",
                          Description ="Fourth Description", DescriptionToDisplay ="Fourth Descriptio...",
                          StartDate =DateTime.Parse("2019/4/2"), EndDate=DateTime.Parse("2019/10/28"),
                          PercentageOfCompletion=56, Priority=2, Status=3 },
                new ToDo{ TablePosition=8, Topic="Fifth Topic", TopicToDisplay="Fifth Topic",
                          Activity ="Fifth Activity", ActivityToDisplay ="Fifth Activity",
                          Description ="Fifth Description", DescriptionToDisplay ="Fifth Description",
                          StartDate =DateTime.Parse("2019/5/2"), EndDate=DateTime.Parse("2019/11/28"),
                          PercentageOfCompletion=62, Priority=2, Status=2 },
                new ToDo{ TablePosition=2, Topic="Sixth Topic", TopicToDisplay="Sixth Topic",
                          Activity ="Sixth Activity", ActivityToDisplay ="Sixth Activity",
                          Description ="Sixth Description", DescriptionToDisplay ="Sixth Description",
                          StartDate =DateTime.Parse("2019/6/2"), EndDate=DateTime.Parse("2019/12/28"),
                          PercentageOfCompletion=12, Priority=3, Status=2 },
                new ToDo{ TablePosition=4, Topic="Seventh Topic", TopicToDisplay="Seventh Topic",
                          Activity ="Seventh Activity", ActivityToDisplay ="Seventh Activity",
                          Description ="Seventh Description", DescriptionToDisplay ="Seventh Descripti...",
                          StartDate =DateTime.Parse("2019/1/2"), EndDate=DateTime.Parse("2019/7/28"),
                          PercentageOfCompletion=72, Priority=3, Status=5 },
                new ToDo{ TablePosition=1, Topic="Eight Topic", TopicToDisplay="Eight Topic",
                          Activity ="Eight Activity", ActivityToDisplay ="Eight Activity",
                          Description ="Eight Description", DescriptionToDisplay ="Eight Description",
                          StartDate =DateTime.Parse("2019/2/2"), EndDate=DateTime.Parse("2019/8/28"),
                          PercentageOfCompletion=67, Priority=2, Status=4 },
                new ToDo{ TablePosition=5, Topic="Ninth Topic", TopicToDisplay="Ninth Topic",
                          Activity ="Ninth Activity", ActivityToDisplay ="Ninth Activity",
                          Description ="Ninth Description", DescriptionToDisplay ="Ninth Description",
                          StartDate =DateTime.Parse("2019/3/2"), EndDate=DateTime.Parse("2019/9/28"),
                          PercentageOfCompletion=76, Priority=3, Status=2 },
                new ToDo{ TablePosition=13, Topic="Tenth Topic", TopicToDisplay="Tenth Topic",
                          Activity ="Tenth Activity", ActivityToDisplay ="Tenth Activity",
                          Description ="Tenth Description", DescriptionToDisplay ="Tenth Description",
                          StartDate =DateTime.Parse("2019/4/2"), EndDate=DateTime.Parse("2019/10/28"),
                          PercentageOfCompletion=44, Priority=5, Status=8 },
                new ToDo{ TablePosition=11, Topic="Eleventh Topic", TopicToDisplay="Eleventh Topic",
                          Activity ="Eleventh Activity", ActivityToDisplay ="Eleventh Activity",
                          Description ="Eleventh Description", DescriptionToDisplay ="Eleventh Descrip...",
                          StartDate =DateTime.Parse("2019/5/2"), EndDate=DateTime.Parse("2019/11/28"),
                          PercentageOfCompletion=56, Priority=6, Status=3 },
                new ToDo{ TablePosition=12, Topic="Tvelveth Topic", TopicToDisplay="Tvelveth Topic",
                          Activity ="Tvelveth Activity", ActivityToDisplay ="Tvelveth Activity",
                          Description ="Tvelveth Description", DescriptionToDisplay ="Tvelveth Descript...",
                          StartDate =DateTime.Parse("2018/7/1"), EndDate=DateTime.Parse("2019/12/28"),
                          PercentageOfCompletion=83, Priority=4, Status=2 },
                new ToDo{ TablePosition=14, Topic="Therteenth Topic", TopicToDisplay="Therteenth Topic",
                          Activity ="Therteenth Activity", ActivityToDisplay ="Therteenth Activity",
                          Description ="Therteenth Description", DescriptionToDisplay ="Therteenth Descri...",
                          StartDate =DateTime.Parse("2019/6/22"), EndDate=DateTime.Parse("2019/11/28"),
                          PercentageOfCompletion=56, Priority=5, Status=8 },
                new ToDo{ TablePosition=17, Topic="Fourteenth Topic", TopicToDisplay="Fourteenth Topic",
                          Activity ="Fourteenth Activity", ActivityToDisplay ="Fourteenth Activity",
                          Description ="Fourteenth Description", DescriptionToDisplay ="Fourteenth Descri...",
                          StartDate =DateTime.Parse("2019/6/2"), EndDate=DateTime.Parse("2019/9/18"),
                          PercentageOfCompletion=48, Priority=4, Status=7 },
                new ToDo{ TablePosition=11, Topic="Fifteenth Topic", TopicToDisplay="Fifteenth Topic",
                          Activity ="Fifteenth Activity", ActivityToDisplay ="Fifteenth Activity",
                          Description ="Fifteenth Description", DescriptionToDisplay ="Fifteenth Descript...",
                          StartDate =DateTime.Parse("2019/6/2"), EndDate=DateTime.Parse("2019/8/8"),
                          PercentageOfCompletion=93, Priority=3, Status=6 },
                new ToDo{ TablePosition=10, Topic="Sixteenth Topic",  TopicToDisplay="Sixteenth Topic",
                          Activity ="Sixteenth Activity", ActivityToDisplay ="Sixteenth Activity",
                          Description ="Sixteenth Description", DescriptionToDisplay ="Sixteenth Descript...",
                          StartDate =DateTime.Parse("2019/5/22"), EndDate=DateTime.Parse("2019/11/8"),
                          PercentageOfCompletion=43, Priority=4, Status=2 }
            };

            todos.ForEach(t => context.ToDos.Add(t));
            context.SaveChanges();
        }
    }
}