﻿using System;
using System.Collections.Generic;
using UI.Models;

namespace UI.DAL
{
    public interface ITodoEfRepository: IRepository<ToDo>
    {
        IEnumerable<ToDo> FindItemsWithDate(string date);
        IEnumerable<ToDo> FindItemsWithProperties(string date);
    }

}