﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UI.Models;

namespace UI.DAL
{
    public static class TodoByHandRepo
    {
        private static readonly List<ToDo> _items = new List<ToDo>
        {
                new ToDo{ Topic="First", Activity="First", Description="First Todo",
                          StartDate =DateTime.Parse("2019/1/2"), EndDate=DateTime.Parse("2019/7/28"),
                          PercentageOfCompletion=0, Priority=1, Status=1 }, 
                new ToDo{ Topic="Second", Activity="Second", Description="Second ToDo",
                          StartDate =DateTime.Parse("2019/2/2"), EndDate=DateTime.Parse("2019/8/28"),
                          PercentageOfCompletion=0, Priority=1, Status=1 },
                new ToDo{ Topic="Thiird", Activity="Third", Description="Third ToDo",
                          StartDate =DateTime.Parse("2019/3/2"), EndDate=DateTime.Parse("2019/9/28"),
                          PercentageOfCompletion=0, Priority=2, Status=2 },
                new ToDo{ Topic="Fourth", Activity="Fourth", Description="Fourth Todo",
                          StartDate =DateTime.Parse("2019/4/2"), EndDate=DateTime.Parse("2019/10/28"),
                          PercentageOfCompletion=0, Priority=2, Status=1 },
                new ToDo{ Topic="Fifth", Activity="Fifth", Description="Fifth ToDo",
                          StartDate =DateTime.Parse("2019/5/2"), EndDate=DateTime.Parse("2019/11/28"),
                          PercentageOfCompletion=0, Priority=3, Status=2 }, 
                new ToDo{ Topic="Sixth", Activity="Sixth", Description="Sixth ToDo",
                          StartDate =DateTime.Parse("2019/6/2"), EndDate=DateTime.Parse("2019/12/28"),
                          PercentageOfCompletion=0, Priority=3, Status=2 }, 
                new ToDo{ Topic="Seventh", Activity="Seventh", Description="Seventh Todo",
                          StartDate =DateTime.Parse("2019/1/2"), EndDate=DateTime.Parse("2019/7/28"),
                          PercentageOfCompletion=0, Priority=2, Status=3 }, 
                new ToDo{ Topic="Eight", Activity="Eight", Description="Eight ToDo",
                          StartDate =DateTime.Parse("2019/2/2"), EndDate=DateTime.Parse("2019/8/28"),
                          PercentageOfCompletion=0, Priority=2, Status=4 }, 
                new ToDo{ Topic="Ninth", Activity="Ninth", Description="Ninth ToDo",
                          StartDate =DateTime.Parse("2019/3/2"), EndDate=DateTime.Parse("2019/9/28"),
                          PercentageOfCompletion=0, Priority=3, Status=2 }, 
                new ToDo{ Topic="Tenth", Activity="Tenth", Description="Tenth Todo",
                          StartDate =DateTime.Parse("2019/4/2"), EndDate=DateTime.Parse("2019/10/28"),
                          PercentageOfCompletion=0, Priority=5, Status=8 }, 
                new ToDo{ Topic="Eleventh", Activity="Eleventh", Description="Eleventh ToDo",
                          StartDate =DateTime.Parse("2019/5/2"), EndDate=DateTime.Parse("2019/11/28"),
                          PercentageOfCompletion=0, Priority=6, Status=3 }, 
                new ToDo{ Topic="Tvelveth", Activity="Tvelveth", Description="Tvelveth ToDo",
                          StartDate =DateTime.Parse("2019/6/2"), EndDate=DateTime.Parse("2019/12/28"),
                          PercentageOfCompletion=0, Priority=4, Status=2 }
            };

        /*
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (1, 'First Activity', 'First Topic', 'First Description' , '2019-1-2', '2019-7-28', 2, 4, 15)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (2, 'Second Activity', 'Second Topic', 'Second Description' , '2019-2-1', '2019-8-15', 1, 2, 35)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (3, 'Third Activity', 'Third Topic', 'Third Description' , '2019-3-1', '2019-6-15', 1, 1, 55)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (4, 'Fourth Activity', 'Fourth Topic', 'Fourth Description' , '2019-4-2', '2019-8-28', 2, 7, 34)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (5, 'Fifth Activity', 'Fifth Topic', 'Fifth Description' , '2019-5-2', '2019-9-28', 3, 5, 15)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (6, 'Sixth Activity', 'Sixth Topic', 'Sixth Description' , '2019-6-2', '2019-7-28', 6, 6, 66)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (7, 'Seventh Activity', 'Seventh Topic', 'Seventh Description' , '2019-7-1', '2019-7-28', 7, 7, 77)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (8, 'Eighth Activity', 'Eighth Topic', 'Eighth Description' , '2019-8-2', '2019-8-28', 8, 8, 88)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (9, 'Ninth Activity', 'Ninth Topic', 'Ninth Description' , '2019-9-2', '2019-10-28', 9, 9, 95)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (10, 'Tenth Activity', 'Tenth Topic', 'Tenth Description' , '2019-10-2', '2019-11-28', 10, 10, 15)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (11, 'Eleventh Activity', 'Eleventh Topic', 'Eleventh Description' , '2019-11-2', '2019-12-28', 11, 4, 25)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (12, 'Twelfth Activity', 'Twelfth Topic', 'Twelfth Description' , '2019-12-2', '2020-7-28', 12, 6, 35)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (13, 'Thirteenth Activity', 'Thirteenth Topic', 'Thirteenth Description' , '2019-3-13', '2019-7-28', 13, 2, 45)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (14, 'Fourteenth Activity', 'Fourteenth Topic', 'Fourteenth Description' , '2019-5-14', '2019-7-28', 14, 1, 55)");
            Sql("Insert into ToDo (TablePosition, Activity, Topic, Description, StartDate, EndDate, Status, Priority, PercentageOfCompletion ) " +
                "values (15, 'Fifteenth Activity', 'Fifteenth Topic', 'Fifteenth Description' , '2019-1-15', '2019-3-28', 15, 8, 45)");
         * */

        static TodoByHandRepo()
        {
            foreach( var i in _items)
            {
                i.CreatePropertiesDictionary();
            }
        }

        public static List<ToDo> Items { get { return _items.OrderBy(t => t.TablePosition).ToList(); } private set{ } }
        
        /*
        public static IEnumerable<ToDo> Enumerate()
        {
            foreach(var item in _items)
            {
                yield return item;
            }
        }
        */
    }
}