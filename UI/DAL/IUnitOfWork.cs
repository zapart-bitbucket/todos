﻿
namespace UI.DAL
{
    public interface IUnitOfWork
    {
        ITodoEfRepository Todos { get; }
        int Process();
    }
}