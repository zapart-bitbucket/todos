﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UI.Models;

namespace UI.DAL
{
    public class TodoEfRepository: EfRepository<ToDo>, ITodoEfRepository
    {
        public TodoEfRepository(DbContext context):base(context)
        { }

        IEnumerable<ToDo> ITodoEfRepository.FindItemsWithDate(string str)
        {
            return _context.Set<ToDo>().AsEnumerable().Where(t => ((DateTime)(t.StartDate)).ToShortDateString().ToLower().Contains(str) ||
                                                   ((DateTime)(t.EndDate)).ToShortDateString().ToLower().Contains(str))
                                       .ToList();
        }

        IEnumerable<ToDo> ITodoEfRepository.FindItemsWithProperties(string str)
        {
            return _context.Set<ToDo>().Where(t => t.Activity.ToLower().Contains(str) ||
                                               t.Description.ToLower().Contains(str) ||
                                               t.Topic.ToLower().Contains(str) ||
                                               t.Priority.ToString().ToLower().Contains(str) ||
                                               t.Status.ToString().ToLower().Contains(str) ||
                                               t.PercentageOfCompletion.ToString().ToLower().Contains(str))
                                      .ToList();
        }
    }
}