﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using UI.Models;

namespace UI.DAL
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() //: //base("name=LocalMdfFileDBContext")
                                     : base("name=LocalDb_MVC_EF")
        {

        }

        public DbSet<ToDo> ToDos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}