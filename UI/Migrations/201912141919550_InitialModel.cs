﻿namespace UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ToDo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TablePosition = c.Int(nullable: false),
                        Activity = c.String(nullable: false, maxLength: 255),
                        ActivityToDisplay = c.String(maxLength: 255),
                        Topic = c.String(nullable: false, maxLength: 255),
                        TopicToDisplay = c.String(maxLength: 255),
                        Description = c.String(nullable: false, maxLength: 255),
                        DescriptionToDisplay = c.String(maxLength: 255),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        Priority = c.Int(nullable: false),
                        PercentageOfCompletion = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ToDo");
        }
    }
}
