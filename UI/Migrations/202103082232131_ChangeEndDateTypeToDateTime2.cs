﻿namespace UI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeEndDateTypeToDateTime2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ToDo", "EndDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ToDo", "EndDate", c => c.DateTime(nullable: false));
        }
    }
}
