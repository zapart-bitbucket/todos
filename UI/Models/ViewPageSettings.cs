﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class ViewPageSettings
    {
        public IPagedList<ToDo> PagedToDoList { get; set; }
        public int PropertyLengthLimit { get; set; }
        public string SearchString { get; set; }
        public ColumnSortInfo ColumnSortInfo { get; set; }
    }
}