﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class ToDo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int TablePosition { get; set; }
        [Required(ErrorMessage = "Please enter Activity")]
        [StringLength(255)]
        public string Activity { get; set; }
        [StringLength(255)]
        public string ActivityToDisplay { get; set; }
        [Required(ErrorMessage = "Please enter Topic")]
        [StringLength(255)]
        public string Topic{ get; set; }
        [StringLength(255)]
        public string TopicToDisplay { get; set; }
        [Required(ErrorMessage = "Please enter Description")]
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(255)]
        public string DescriptionToDisplay { get; set; }
        [Column(TypeName = "datetime2")]
        [Required(ErrorMessage = "Please enter Start Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? StartDate { get; set; }
        [Column(TypeName = "datetime2")]
        [Required(ErrorMessage = "Please enter End Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? EndDate { get; set; }
        [Required(ErrorMessage = "Please choose Status")]
        [Range(1,10)]
        public int? Status { get; set; }
        [Required(ErrorMessage = "Please choose Priority")]
        [Range(1,10)]
        public int? Priority { get; set; }
        [Required(ErrorMessage = "Please enter % of completion ")]
        [Display(Name = "%Complete")]
        [Range(0, 100)]
        public int? PercentageOfCompletion { get; set; }
        

        private Dictionary<string, string> PropertiesDictionary = 
            new Dictionary<string, string>();

        
        public void CreatePropertiesDictionary()
        {
            if (PropertiesDictionary.Count == 0 )
            {
                var type = this.GetType();
                foreach (var prop in type.GetProperties())
                {
                    if (prop.Name != "Item")
                    {
                        var val = prop.GetValue(this, new object[] { });
                        var valStr = val == null ? "" : val.ToString();
                        PropertiesDictionary.Add(prop.Name, valStr);
                    }
                }
            }
        }

        public Dictionary<string, string> GetPropertiesDictionary()
        {
            return PropertiesDictionary;
        }

        public string this[string index]
        {
            get
            {
                if (PropertiesDictionary != null && PropertiesDictionary.Count > 0 && PropertiesDictionary.Keys.Contains(index))
                {
                    return PropertiesDictionary[index];
                }
                else
                {
                    throw new InvalidOperationException("Unknown PropertiesDictionary Key requested.");
                }
            }
        }

    }
}