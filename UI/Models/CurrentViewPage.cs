﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class CurrentViewPage
    {
        public ViewPageType Current { get; set; }
        public ViewPageType Before { get; set; }

    }
}