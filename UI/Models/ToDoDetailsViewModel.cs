﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class ToDoDetailsViewModel
    {
        public ToDo ToDo { get; set; }
        public string CallFrom { get; set; }
        public int PageNumber { get; set; }
    }
}