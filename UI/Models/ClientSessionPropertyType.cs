﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public enum ClientSessionPropertyType
    {
        TodoList, TodoTiles, SearchString, CurrentViewPage, ColumnSortInfo, TilesSearchPattern,
        ListPageNumber, TilesPageNumber, ListPageSize, TilesPageSize
    }
}