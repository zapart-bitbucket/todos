﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public class ClientSession
    {
        public static  List<ToDo> TodoList
        {
            get
            {
                return (List<ToDo>)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TodoList.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TodoList.ToString()] = value;
            }
        }

        public static List<ToDo> TodoTiles
        {
            get
            {
                return (List<ToDo>)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TodoTiles.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TodoTiles.ToString()] = value;
            }
        }

        public static string SearchString
        {
            get
            {
                return (string)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.SearchString.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.SearchString.ToString()] = value;
            }
        }

        public static CurrentViewPage CurrentViewPage
        {
            get
            {
                return (CurrentViewPage)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.CurrentViewPage.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.CurrentViewPage.ToString()] = value;
            }
        }

        public static ColumnSortInfo ColumnSortInfo
        {
            get
            {
                return (ColumnSortInfo)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ColumnSortInfo.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ColumnSortInfo.ToString()] = value;
            }
        }

        public static TilesSearchViewModel TilesSearchPattern
        {
            get
            {
                return (TilesSearchViewModel)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesSearchPattern.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesSearchPattern.ToString()] = value;
            }
        }

        public static int? ListPageNumber
        {
            get
            {
                return (int?)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ListPageNumber.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ListPageNumber.ToString()] = value;
            }
        }

        public static int? ListPageSize
        {
            get
            {
                return (int?)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ListPageSize.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.ListPageSize.ToString()] = value;
            }
        }

        public static int? TilesPageNumber
        {
            get
            {
                return (int?)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesPageNumber.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesPageNumber.ToString()] = value;
            }
        }

        public static int? TilesPageSize
        {
            get
            {
                return (int?)System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesPageSize.ToString()];
            }
            set
            {
                System.Web.HttpContext.Current.Session[ClientSessionPropertyType.TilesPageSize.ToString()] = value;
            }
        }

        public static void SetIntPageProperty(string property, int? value)
        {
            System.Web.HttpContext.Current.Session[((ClientSessionPropertyType)Enum.Parse(typeof(ClientSessionPropertyType), property)).ToString()] = value;
        }

        public static int? GetIntPageProperty(string property)
        {
            return (int?)System.Web.HttpContext.Current.Session[((ClientSessionPropertyType)Enum.Parse(typeof(ClientSessionPropertyType), property)).ToString()];
        }

    }
}