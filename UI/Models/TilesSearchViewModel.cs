﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using PagedList.Mvc;

namespace UI.Models
{
    public class TilesSearchViewModel
    {
        public IPagedList<ToDo> PagedToDoList { get; set; }
        public ToDo ToDoToSearch { get; set; }
        public int PropertyLengthLimit { get; set; }
        public LogicalOperator LogicalOperator { get; set; }

        private Dictionary<string, string> SearchPropertiesDictionary;

        public TilesSearchViewModel()
        {
            //PropertiesDictionary = GetValueProperties();
        }

        public Dictionary<string, string> GetValidSearchProperties()
        {
            var validProps = new Dictionary<string, string>();

            GetSearchPropertiesDictionary();

            foreach(var p in SearchPropertiesDictionary)
            {
                if( !string.IsNullOrWhiteSpace(this[p.Key])  )
                {
                    validProps.Add(p.Key, p.Value);
                }
            }
            return validProps;
        }

        

        private void GetSearchPropertiesDictionary()
        {
            if( ToDoToSearch == null )
            {
                throw new NullReferenceException("ToSearch subobject of TilesSearchViewModel is null");
            }

            if (SearchPropertiesDictionary == null)
            {
                SearchPropertiesDictionary = new Dictionary<string, string>();
                
                var type = this.ToDoToSearch.GetType();
                foreach (var prop in type.GetProperties())
                {
                    if (prop.Name != "Item" && prop.Name != "TablePosition" && prop.Name != "Id" )
                    {
                        var val = prop.GetValue(this.ToDoToSearch, new object[] { });
                        var valStr = val == null ? "" : val.ToString();
                        SearchPropertiesDictionary.Add(prop.Name, valStr);
                    }
                }
            }
        }

        public string this[string index]
        {
            get
            {
                if (SearchPropertiesDictionary != null)
                {
                    return SearchPropertiesDictionary[index];
                }
                else
                {
                    return "";
                }
            }
        }
    }
}