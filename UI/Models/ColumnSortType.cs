﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UI.Models
{
    public enum ColumnSortType
    {
        NotSorted=0, Ascending, Descending
    }
}