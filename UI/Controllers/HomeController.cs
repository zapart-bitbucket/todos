﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UI.Models;
using PagedList;
using System.Text;
using UI.DAL;
using System.Data.SqlClient;

namespace UI.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private static int _listDefaultPageSize = 10;
        private static int _tilesDefaultPageSize = 5;
        private static int _propertyLengthLimit = 20; 
        
        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

            InitSessionTodoList();
            InitColumnSortInfo();
            CreatePropertiesDictionaryForAllTodos();
        }

        private void InitSessionTodoList()
        {
            ClientSession.TodoList = _unitOfWork.Todos.GetAll().OrderBy(t=>t.TablePosition).ToList();
        }


        private void InitColumnSortInfo()
        {
            if (ClientSession.ColumnSortInfo == null)
            {
                ClientSession.ColumnSortInfo = new ColumnSortInfo {
                    SortedColumnName = "Empty",
                    ColumnSortType = ColumnSortType.NotSorted
                }; 
            }
        }

        
        // GET: Home
        public ActionResult Index(int? pageNumber, int? pageSize, string searchString, string columnNameSort) 
        {
            InitSessionTodoList();

            CreatePropertiesDictionaryForAllTodos();

            SetPageSize(pageSize, TodoDisplayType.ListDisplay);

            CopyTodosFromEarlierViewToCurrent(ViewPageType.List);

            SetSessionSearchStringBasedOnCurrentSearchString(searchString);

            SearchTodosForSearchString(ClientSession.SearchString); 
                           
            SortTodosByColumn(columnNameSort);

            var pageNmbr = GetPageNumber(pageNumber, TodoDisplayType.ListDisplay); 
            var pageSettings = SetViewPageSettings(pageNmbr, ClientSession.SearchString);

            return View(pageSettings);
        }

              

        private void SetPageSize(int? pageSize, TodoDisplayType displayType)
        {
            string pageSizeFor = displayType == TodoDisplayType.ListDisplay ? "ListPageSize" : "TilesPageSize";
           
            if (pageSize.HasValue)
            {
                ClientSession.SetIntPageProperty(pageSizeFor, pageSize);
            }
            else
            {
                if (displayType == TodoDisplayType.ListDisplay)
                {
                    if (ClientSession.GetIntPageProperty(pageSizeFor) == null)
                    {
                        ClientSession.SetIntPageProperty(pageSizeFor, _listDefaultPageSize);
                    }
                }
                else
                {
                    if (ClientSession.GetIntPageProperty(pageSizeFor) == null)
                    {
                        ClientSession.SetIntPageProperty(pageSizeFor, _tilesDefaultPageSize);
                    }
                }
            }
        }

        

        private void CopyTodosFromEarlierViewToCurrent(ViewPageType currentViewPageType)
        {
            if (ClientSession.CurrentViewPage == null)
            {
                ClientSession.CurrentViewPage = new CurrentViewPage
                {
                    Current = ViewPageType.List,
                    Before = ViewPageType.List
                };
            }
            else
            {
                if (currentViewPageType == ViewPageType.List &&
                    ClientSession.CurrentViewPage.Before == ViewPageType.Tiles)
                {
                    ClientSession.TodoList = ClientSession.TodoTiles;
                    var newViewPage = new CurrentViewPage { Current = ViewPageType.List, Before = ViewPageType.List };
                    ClientSession.CurrentViewPage = newViewPage;
                }

                if (currentViewPageType == ViewPageType.Tiles &&
                    ClientSession.CurrentViewPage.Before == ViewPageType.List)
                {
                    SearchTodosForSearchString(ClientSession.SearchString); 
                    ClientSession.TodoTiles = ClientSession.TodoList;
                    var newViewPage = new CurrentViewPage { Current = ViewPageType.Tiles, Before = ViewPageType.Tiles };
                    ClientSession.CurrentViewPage = newViewPage;
                }
            }
        }


        private int GetPageNumber(int? pageNumber, TodoDisplayType displayType)
        {
            var displayTypePrefix = displayType == TodoDisplayType.ListDisplay ? "List" : "Tiles";
            var pageNumberPostfix = "PageNumber";
            var pageSizePostfix = "PageSize";

            if (pageNumber.HasValue)
            {
                ClientSession.SetIntPageProperty(displayTypePrefix + pageNumberPostfix, pageNumber);
            }
            else
            {
                if (ClientSession.GetIntPageProperty(displayTypePrefix + pageNumberPostfix) == null)
                {
                    ClientSession.SetIntPageProperty(displayTypePrefix + pageNumberPostfix, pageNumber ?? 1);
                }
                else if (ClientSession.GetIntPageProperty(displayTypePrefix + pageSizePostfix).Value *
                          ClientSession.GetIntPageProperty(displayTypePrefix + pageNumberPostfix).Value > ClientSession.TodoList.Count)
                {
                    var pageSize = ClientSession.GetIntPageProperty(displayTypePrefix + pageSizePostfix).Value;
                    var pageNumbr = ClientSession.GetIntPageProperty(displayTypePrefix + pageNumberPostfix).Value;
                    var maxPageNumber = ClientSession.TodoList.Count / pageSize;
                    if (ClientSession.TodoList.Count % pageSize > 0)
                    {
                        maxPageNumber++;
                    }
                    ClientSession.SetIntPageProperty(displayTypePrefix + pageNumberPostfix, maxPageNumber);
                }
            }

            var pageNmbr = ClientSession.GetIntPageProperty(displayTypePrefix + pageNumberPostfix).Value;
            if (pageNmbr < 1)
            {
                pageNmbr = 1;
            }

            return pageNmbr;
        }


        private void SetSessionSearchStringBasedOnCurrentSearchString(string searchString)
        {
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                ClientSession.SearchString = searchString;
                ClearTilesSearchPattern();
            }

            if (!string.IsNullOrEmpty(searchString) && 
                searchString.Trim() == string.Empty && 
                !string.IsNullOrWhiteSpace(ClientSession.SearchString))
            {
                ClientSession.SearchString = string.Empty;
            }
        }


        private void SortTodosByColumn(string columnName)
        {
            List<ToDo> sorted = null;
            bool doChangeSortType = true;
            var currentColumnName = columnName;

            if (string.IsNullOrWhiteSpace(columnName))
            {
                columnName = ClientSession.ColumnSortInfo.SortedColumnName;
                doChangeSortType = false;
            }

            if (columnName == "Empty" || ClientSession.TodoList.Count < 2)
            {
                return;
            }

            var columnSortInfo = ClientSession.ColumnSortInfo;
            if( doChangeSortType == true && columnSortInfo.ColumnSortType != ColumnSortType.NotSorted )
            {
                if (currentColumnName != columnSortInfo.SortedColumnName)
                {
                    ClientSession.ColumnSortInfo.ColumnSortType = ColumnSortType.NotSorted;
                }
            }

            var isIntParseOk = int.TryParse( ClientSession.TodoList[0][columnName], out int parsedValue );
            if (isIntParseOk)
            {
                sorted = SortByNumberColumnDependOnColumnSortType(columnName, columnSortInfo, doChangeSortType);
            }
            else
            {
                sorted = SortByNotNumberColumnDependOnColumnSortType(columnName, columnSortInfo, doChangeSortType);
            }
            
            if (sorted != null)
            {
                ClientSession.TodoList = sorted.ToList();
                SetCorrectTablePositionForAllClientTodos();
            }
        }


        private List<ToDo> SortAscendingByNumberValuedColumn(string columnName)
        {
            return ClientSession.TodoList.OrderBy(a => a[columnName].Length).ThenBy(a => a[columnName]).ToList();
        }

        private List<ToDo> SortDescendingByNumberValuedColumn(string columnName)
        {
            return ClientSession.TodoList.OrderByDescending(a => a[columnName].Length).ThenByDescending(a => a[columnName]).ToList();
        }

        private List<ToDo> SortAscendingByNotNumberValuedColumn(string columnName)
        {
            return ClientSession.TodoList.OrderBy(a => a[columnName]).ToList();
        }

        private List<ToDo> SortDescendingByNotNumberValuedColumn(string columnName)
        {
            return ClientSession.TodoList.OrderByDescending(a => a[columnName]).ToList();
        }

                
        
        private List<ToDo> SortByNumberColumnDependOnColumnSortType(string columnName, ColumnSortInfo earlierColumnSortInfo, bool changeSortType)
        {
            List<ToDo> sorted = null;

            switch (earlierColumnSortInfo.ColumnSortType)
            {
                case ColumnSortType.Ascending:
                    if (changeSortType)
                    {
                        sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(true, true, columnName, earlierColumnSortInfo);
                    }
                    else
                    {
                        sorted = SortAscendingByNumberValuedColumn(columnName);
                    }
                    break;
                case ColumnSortType.Descending:
                    if (changeSortType)
                    {
                        sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(true, false, columnName, earlierColumnSortInfo);
                    }
                    else
                    {
                        sorted = SortDescendingByNumberValuedColumn(columnName);
                    }
                    break;
                default:
                    if (changeSortType)
                    {
                         sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(true, false, columnName, earlierColumnSortInfo);
                    }
                    else
                    {
                        sorted = SortAscendingByNumberValuedColumn(columnName);
                    }
                    break;
            };

            return sorted;
        }

        

        private List<ToDo> SortByNotNumberColumnDependOnColumnSortType(string columnName, ColumnSortInfo columnSortInfo, bool changeSortType)
        {
            List<ToDo> sorted = null;

            switch (columnSortInfo.ColumnSortType)
            {
                case ColumnSortType.Ascending:
                    if (changeSortType)
                    {
                        sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(false, true, columnName, columnSortInfo);
                    }
                    else
                    {
                        sorted = SortAscendingByNotNumberValuedColumn(columnName);
                    }
                    break;
                case ColumnSortType.Descending:
                    if (changeSortType)
                    {
                        sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(false, false, columnName, columnSortInfo);
                    }
                    else
                    {
                        sorted = SortDescendingByNotNumberValuedColumn(columnName);
                    }
                    break;
                default:
                    if (changeSortType)
                    {
                       sorted = SortTodoByColumnTypeAndSortTypeAndDoChangeSort(false, false, columnName, columnSortInfo);
                    }
                    else
                    {
                        sorted = SortAscendingByNotNumberValuedColumn(columnName);
                    }
                    break;
            };

            return sorted;
        }


        private List<ToDo> SortTodoByColumnTypeAndSortTypeAndDoChangeSort(bool isNumberColumn,
                                                                          bool isAscSort,
                                                                          string columnName,
                                                                          ColumnSortInfo columnSortInfo)
        {
            List<ToDo> sorted = null;

            if (isNumberColumn)
            {
                if (isAscSort)
                {
                    sorted = SortDescendingByNumberValuedColumn(columnName);
                    columnSortInfo.ColumnSortType = ColumnSortType.Descending;
                }
                else // desc sort
                {
                    sorted = SortAscendingByNumberValuedColumn(columnName);
                    columnSortInfo.ColumnSortType = ColumnSortType.Ascending;
                }
            }
            else // not numberColumn
            {
                if (isAscSort)
                {
                    sorted = SortDescendingByNotNumberValuedColumn(columnName);
                    columnSortInfo.ColumnSortType = ColumnSortType.Descending;
                }
                else // desc sort
                {
                    sorted = SortAscendingByNotNumberValuedColumn(columnName);
                    columnSortInfo.ColumnSortType = ColumnSortType.Ascending;
                }
            }

            columnSortInfo.SortedColumnName = columnName;
            return sorted;
        }


        private void SearchTodosForSearchString(string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString))
            {
                InitSessionTodoList();
            }
            else if (searchString == "Tiles search pattern")
            {
                var tilesSearch = ClientSession.TilesSearchPattern;
                var searchFields = tilesSearch.GetValidSearchProperties();
                if (searchFields.Count > 0)
                {
                    ClientSession.TodoList = SearchTodosForTilesPattern(tilesSearch, searchFields);
                }
            }
            else
            {
                ClientSession.TodoList = SearchTodosFor(searchString);
            }
        }


        private void ClearColumnSorting()
        {
            ClientSession.ColumnSortInfo = new ColumnSortInfo { SortedColumnName = "Empty", ColumnSortType = ColumnSortType.NotSorted };
        }


        private void ClearTilesSearchPattern()
        {
            var tilesSearchPattern = ClientSession.TilesSearchPattern;
            if (tilesSearchPattern != null)
            {
                tilesSearchPattern.ToDoToSearch = new ToDo();
                ClientSession.TilesSearchPattern = tilesSearchPattern;
            }
        }


        private ViewPageSettings SetViewPageSettings(int pageNmbr, string searchString)
        {
            var listPageSize = (int)ClientSession.ListPageSize;

            return new ViewPageSettings()
            {
                PagedToDoList = ClientSession.TodoList.ToPagedList(pageNmbr, listPageSize),
                PropertyLengthLimit = _propertyLengthLimit,
                ColumnSortInfo = ClientSession.ColumnSortInfo,
                SearchString = searchString
            };
        }

        
        private List<ToDo> SearchTodosFor(string str)
        {
            var selected = _unitOfWork.Todos.FindItemsWithProperties(str).ToList();
            var fromDate = _unitOfWork.Todos.FindItemsWithDate(str);
                                   
            foreach(var t in fromDate)
            {
                if (!selected.Contains(t))
                {
                    selected.Add(t);
                }
            }
            
            return selected;
        }

        
        public ActionResult ExportToFile( string fileType )
        {
            var sb = new StringBuilder();

            if (ClientSession.TodoList.Count > 0)
            {
                foreach (var i in ClientSession.TodoList[0].GetPropertiesDictionary())
                {
                    sb.Append(i.Key + ",");
                }

                sb.Append(Environment.NewLine);

                foreach (var t in ClientSession.TodoList)
                {
                    sb.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}",
                                    t.Id,
                                    t.TablePosition,     
                                    t.Activity,
                                    t.Topic,
                                    t.Description,
                                    ((DateTime)t.StartDate).ToShortDateString(),
                                    ((DateTime)t.EndDate).ToShortDateString(),
                                    t.Status,
                                    t.Priority,
                                    t.PercentageOfCompletion,
                                    Environment.NewLine);
                }

                var response = System.Web.HttpContext.Current.Response;
                response.BufferOutput = true;
                response.Clear();
                response.ClearHeaders();
                response.ContentEncoding = Encoding.Unicode;
                if (fileType == "CSV")
                {
                    response.AddHeader("content-disposition", "attachment;filename=Todos.CSV ");
                }
                else if (fileType == "TXT")
                {
                    response.AddHeader("content-disposition", "attachment;filename=Todos.TXT ");
                }
                response.ContentType = "text/plain";
                response.Write(sb.ToString());
                response.End();
            }
            
            return RedirectToAction("Index");
        }


        public ActionResult Tiles(int? pageNumber, int? pageSize, string displayMode)  
        {
            SetPageSize(pageSize, TodoDisplayType.TilesDisplay);
            CopyTodosFromEarlierViewToCurrent(ViewPageType.Tiles);
            int pageNmbr = GetPageNumber(pageNumber, TodoDisplayType.TilesDisplay);
            var tilesPageSize = (int)ClientSession.TilesPageSize;
            if (displayMode != null && displayMode == "all" ) 
            {
                ClientSession.TodoTiles = _unitOfWork.Todos.GetAll().ToList();
                ClientSession.SearchString = string.Empty;
                ClearTilesSearchPattern();
                var tilesSettings = PopulateTilesSearchViewModel(1, tilesPageSize, new ToDo(), LogicalOperator.Or);
                return View(tilesSettings);
            };

            TilesSearchViewModel currentSearchPattern = ClientSession.TilesSearchPattern;
            if( currentSearchPattern != null )
            {
                var searchFields = currentSearchPattern.GetValidSearchProperties();
                var tilesSettings = PopulateTilesSearchViewModel(pageNmbr, tilesPageSize, 
                                                                 ClientSession.TilesSearchPattern.ToDoToSearch,
                                                                 ClientSession.TilesSearchPattern.LogicalOperator);
                return View(tilesSettings);
            }
            else
            {
                var tilesSettings = PopulateTilesSearchViewModel(pageNmbr, tilesPageSize, new ToDo(), LogicalOperator.Or);
                return View(tilesSettings);
            }
        }


        private TilesSearchViewModel PopulateTilesSearchViewModel(int pageNmbr, 
                                                                 int tilesPageSize, 
                                                                 ToDo todoToSearch, 
                                                                 LogicalOperator logicalOperator)
        {
            var tilesSearchViewModel = new TilesSearchViewModel();

            tilesSearchViewModel.PagedToDoList = ClientSession.TodoTiles.ToPagedList(pageNmbr, tilesPageSize);
            tilesSearchViewModel.PropertyLengthLimit = _propertyLengthLimit;
            tilesSearchViewModel.ToDoToSearch = todoToSearch;
            tilesSearchViewModel.LogicalOperator = logicalOperator;

            return tilesSearchViewModel;
        }

        
        [HttpPost]
        public ActionResult Tiles(TilesSearchViewModel tilesSearch, int? pageNumber)
        {
            var selected = new List<ToDo>();
            var searchFields = tilesSearch.GetValidSearchProperties();
            int pageNmbr = (pageNumber ?? 1);
            
            if (searchFields.Count > 0)
            {
                selected = SearchTodosForTilesPattern(tilesSearch, searchFields);

                ClientSession.TodoTiles = selected;
                ClientSession.TilesSearchPattern = tilesSearch;
                ClientSession.SearchString = "Tiles search pattern";
            }
            else
            {
                ClientSession.TodoTiles = _unitOfWork.Todos.GetAll().ToList(); 
                ClientSession.TilesSearchPattern = tilesSearch;
                ClientSession.SearchString = string.Empty;
            }

            var tilesPageSize = (int)ClientSession.TilesPageSize;
            var tilesSearchViewModel = PopulateTilesSearchViewModel(pageNmbr, tilesPageSize,
                                                               tilesSearch.ToDoToSearch,
                                                               tilesSearch.LogicalOperator);
            return View(tilesSearchViewModel);
        }


        private List<ToDo> SearchTodosForTilesPattern(TilesSearchViewModel tilesSearch, Dictionary<string, string> searchFields)
        {
            var selected = new List<ToDo>();
            
            if (tilesSearch.LogicalOperator == LogicalOperator.Or)
            {
                selected = OrOperatorSearchFor(searchFields);
            }
            else if (tilesSearch.LogicalOperator == LogicalOperator.And)
            {
                selected = AndOperatorSearchFor(searchFields);
            }

            return selected;
        }


        private List<ToDo> OrOperatorSearchFor(Dictionary<string,string> searchFields)
        {
            var selected = new List<ToDo>();

            foreach (var p in searchFields)
            {
                foreach (var t in ClientSession.TodoTiles) 
                {
                    if (t[p.Key].IndexOf(p.Value, StringComparison.InvariantCultureIgnoreCase) >= 0)
                    {
                        if (!selected.Contains(t))
                        {
                            selected.Add(t);
                        }
                    }
                }
            }
                       
            return selected;
        }

        private List<ToDo> AndOperatorSearchFor(Dictionary<string, string> searchFields)
        {
            var selected = new List<ToDo>();

            var matchAll = true;
            foreach (var t in ClientSession.TodoTiles) 
            {
                matchAll = true;
                foreach (var p in searchFields)
                {
                    if (t[p.Key].IndexOf(p.Value, StringComparison.InvariantCultureIgnoreCase) < 0)
                    {
                        matchAll = false;
                        break;
                    }
                }

                if ( matchAll && !selected.Contains(t))
                {
                    selected.Add(t);
                }
            }
                        
            return selected;
        }


        public ActionResult ErrorModalPopup()
        {
            return PartialView();
        }

        public ActionResult Details(int id, int? pageNumber )
        {
            ToDo item = null;

            int pageNmbr = (pageNumber ?? 1);
            if (ClientSession.CurrentViewPage.Current == ViewPageType.List)
            {
                item = ClientSession.TodoList.FirstOrDefault(i => i.Id == id);
            }
            else
            {
                item = ClientSession.TodoTiles.FirstOrDefault(i => i.Id == id);
            }

            var details = new ToDoDetailsViewModel()
            {
                ToDo = item,
                CallFrom = ClientSession.CurrentViewPage.Current.ToString().ToLower(), 
                PageNumber = pageNmbr
            };
            
            return PartialView(details);
        }

        public ActionResult Create()
        {
            return PartialView(new ToDo());
        }

        [HttpPost]
        public ActionResult Create(ToDo todo)
        {
            if (ModelState.IsValid)
            {
                todo.Id = 1;
                todo.ActivityToDisplay = GetLengthLimitedProperty(todo.Activity);
                todo.TopicToDisplay = GetLengthLimitedProperty(todo.Topic);
                todo.DescriptionToDisplay = GetLengthLimitedProperty(todo.Description);
                if (_unitOfWork.Todos.GetAll().Count() == 0)
                {
                    todo.TablePosition = 1;
                }
                else
                {
                    todo.TablePosition = _unitOfWork.Todos.GetAll().Max(t => t.TablePosition) + 2;
                }

                _unitOfWork.Todos.Add(todo); 
                _unitOfWork.Process();

                InitSessionTodoList();

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        public ActionResult Edit(int id, int? pageNumber)
        {
            int pageNmbr = (pageNumber ?? 1);
            var item = ClientSession.TodoList.FirstOrDefault(i => i.Id == id);

            if (ClientSession.CurrentViewPage == null || ClientSession.CurrentViewPage.Current == ViewPageType.None)
            {
                ClientSession.CurrentViewPage = new CurrentViewPage { Current = ViewPageType.List };
            }

            var details = new ToDoDetailsViewModel()
            {
                ToDo = item,
                CallFrom = ClientSession.CurrentViewPage.Current.ToString().ToLower(), 
                PageNumber = pageNmbr
            };

            return PartialView(details);
        }

        [HttpPost]
        public ActionResult EditPost(ToDoDetailsViewModel todoDetails)
        {
            if (ModelState.IsValid)
            {
                var item = _unitOfWork.Todos.Get(todoDetails.ToDo.Id); 

                item.Activity = todoDetails.ToDo.Activity;
                item.ActivityToDisplay = GetLengthLimitedProperty(todoDetails.ToDo.Activity);
                item.Topic = todoDetails.ToDo.Topic;
                item.TopicToDisplay = GetLengthLimitedProperty(todoDetails.ToDo.Topic);
                item.Description = todoDetails.ToDo.Description;
                item.DescriptionToDisplay = GetLengthLimitedProperty(todoDetails.ToDo.Description);
                item.StartDate = todoDetails.ToDo.StartDate;
                item.EndDate = todoDetails.ToDo.EndDate;
                item.Priority = todoDetails.ToDo.Priority;
                item.Status = todoDetails.ToDo.Status;
                item.PercentageOfCompletion = todoDetails.ToDo.PercentageOfCompletion;

                _unitOfWork.Process();

                InitSessionTodoList();

                if(ClientSession.CurrentViewPage.Current == ViewPageType.List)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Tiles");
                }
            }
            else
            {
                return PartialView("~/Views/Home/Edit.cshtml",todoDetails);
            }
        }


        private string GetLengthLimitedProperty(string property)
        {
            if(property.Length > _propertyLengthLimit)
            {
                return property.Substring(0, _propertyLengthLimit-3) + "...";
            }
            else
            {
                return property;
            }
        }
        
        public ActionResult DeleteConfirmation(int id, int? pageNumber)
        {
            int pageNmbr = (pageNumber ?? 1);
            var item = ClientSession.TodoList.FirstOrDefault(i => i.Id == id);

            var details = new ToDoDetailsViewModel()
            {
                ToDo = item,
                CallFrom = ClientSession.CurrentViewPage.Current.ToString().ToLower(),  
                PageNumber = pageNmbr
            };

            return PartialView(details);
        }

        [HttpPost]
        public ActionResult Delete(int id, int? pageNumber)
        {
            int pageNmbr = (pageNumber ?? 1);
            var item = _unitOfWork.Todos.Get(id);

            if (item != null)
            {
                _unitOfWork.Todos.Remove(item);
                _unitOfWork.Process();                
            }

            return RedirectToAction("Index", new { pageNumber = pageNmbr, searchString = ClientSession.SearchString});
        }

        public ActionResult MoveUp(int tablePosition, int? pageNumber)
        {
            int pageNmbr = (pageNumber ?? 1);

            MoveItemTowards(tablePosition, true);

            ClearColumnSorting();

            return RedirectToAction("Index"); 
        }

        

        public ActionResult MoveDown(int tablePosition, int? pageNumber)
        {
            int pageNmbr = (pageNumber ?? 1);

            MoveItemTowards(tablePosition, false);

            ClearColumnSorting();

            return RedirectToAction("Index"); 
        }


        private void MoveItemTowards(int tablePosition, bool up)
        {
            var item = ClientSession.TodoList.FirstOrDefault(i => i.TablePosition == tablePosition);

            if (item != null)
            {
                var itemToMoveDown = GetPreviousOrNextItemFor(item, up);
                SwapItemsClientTablePosition(item, itemToMoveDown);
            }
        }


        private ToDo GetPreviousOrNextItemFor(ToDo item, bool next)
        {
            var itemIndex = ClientSession.TodoList.IndexOf(item);
            var itemsCount = ClientSession.TodoList.Count;

            ToDo itemToMove = null;

            if (next)
            {
                if (itemIndex - 1 >= 0)
                {
                    itemToMove = ClientSession.TodoList[itemIndex - 1];
                }
            }
            else
            {
                if (itemIndex + 1 < ClientSession.TodoList.Count())
                {
                    itemToMove = ClientSession.TodoList[itemIndex + 1];
                }
            }

            return itemToMove;
        }


        private void SwapItemsClientTablePosition(ToDo toMoveUp, ToDo toMoveDown)
        {
            var tempTablePosition = toMoveUp.TablePosition;
            toMoveUp.TablePosition = toMoveDown.TablePosition;
            toMoveDown.TablePosition = tempTablePosition;

            _unitOfWork.Process();
        }


        
        private void SetCorrectTablePositionForAllClientTodos()
        {
            for (int i=0; i<ClientSession.TodoList.Count(); i++)
            {
                ClientSession.TodoList[i].TablePosition = i;
            }

            _unitOfWork.Process();
        }


        

        private void CreatePropertiesDictionaryForAllTodos()
        {
            foreach (var t in ClientSession.TodoList)
            {
                t.CreatePropertiesDictionary();
            }
        }
                

    }
}